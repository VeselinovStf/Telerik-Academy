# Telerik Alpfa Academy November/2017-2018 #
![](https://vx2.thecamp.me/api/collections/download/20000000001450/image/?ts=1511216303399&size=big&size=medium)

> Telerik Academy Alpha is new 6-month program that equips with the necessary knowledge and skills to jumpstart your IT career. Leveraging innovative teaching methods and a personalized training approach mixed with work on real-world projects, our brand-new program will equip you with the same in-demand skills and hands-on experience as our standard program. In twice as short a time.

----------
Content
----------
1. [Module 1 => CSharp Advanced](https://github.com/VeselinovStf/Telerik-Alfa/tree/master/Module_1_CSharp_Advanced "Module 1 => C# Advanced")

	- [Multidimensional Arrays](https://github.com/VeselinovStf/Telerik-Alpfa/tree/master/Module_1_CSharp_Advanced/Multidimentional_Arrays](https://github.com/VeselinovStf/Telerik-Alpfa/tree/master/Module_1_CSharp_Advanced/Multidimentional_Arrays "Multidimensional Arrays")
	- [Strings and Text Processing](https://github.com/VeselinovStf/Telerik-Alpfa/tree/master/Module_1_CSharp_Advanced/Strings)
	- [Methods](https://github.com/VeselinovStf/Telerik-Alpfa/tree/master/Module_1_CSharp_Advanced/Methods)
	- [Exceptions](https://github.com/VeselinovStf/Telerik-Alpfa/tree/master/Module_1_CSharp_Advanced/ExceptionHandling)
