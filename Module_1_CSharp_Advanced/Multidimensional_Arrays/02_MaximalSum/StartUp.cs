﻿namespace _02_MaximalSum
{
    using System;
    using System.Linq;

    public class StartUp
    {
        public static void Main()
        {
            int[] sizes = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

            int n = sizes[0];
            int m = sizes[1];

            int[,] matrix = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                int[] row = Console.ReadLine()
                    .Split(' ')
                    .Select(int.Parse)
                    .ToArray();

                for (int j = 0; j < row.Length; j++)
                {
                    matrix[i, j] = row[j];
                }
            }


            int maxSum = int.MinValue;

            for (int row = 0; row < matrix.GetLength(0) - 2; row++)
            {
                for (int col = 0; col < matrix.GetLength(1) - 2; col++)
                {
                    int currentSum = matrix[row, col]
                        + matrix[row + 1, col] + matrix[row + 2, col] +
                        matrix[row, col + 1]
                        + matrix[row + 1, col + 1] + matrix[row + 2, col + 1] +
                        matrix[row, col + 2]
                        + matrix[row + 1, col + 2] + matrix[row + 2, col + 2];


                          if (currentSum > maxSum)
                    {
                        maxSum = currentSum;
                    }
                }


            }
            Console.WriteLine(maxSum);
        }
    }
}
