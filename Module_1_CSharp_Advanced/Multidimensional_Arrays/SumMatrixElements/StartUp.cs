﻿namespace SumMatrixElements
{
    using System;
    using System.Linq;

    /// <summary>
    /// Write program that read a matrix from console and print:
    /// •	Count of rows
    /// •	Count of columns
    /// •	Sum of all matrix’s elements
    /// On first line you will get matrix sizes in format[rows, columns]
    /// </summary>
    public class StartUp
    {
        public static void Main()
        {
            string input = Console.ReadLine();

            int[] size = GetInputInArray(input);

            int rows = size[0];
            int cols = size[1];

            int[,] matrix = new int[rows, cols];

            int sum = 0;

            for (int matrixRow = 0; matrixRow < rows; matrixRow++)
            {
                input = Console.ReadLine();
                int[] inputRowOfMatrix = GetInputInArray(input);

                for (int matrixCol = 0; matrixCol < inputRowOfMatrix.Length; matrixCol++)
                {
                    matrix[matrixRow, matrixCol] = inputRowOfMatrix[matrixCol];
                    sum += inputRowOfMatrix[matrixCol];
                }

            }

            Console.WriteLine(rows);
            Console.WriteLine(cols);
            Console.WriteLine(sum);

        }

   
        private static int[] GetInputInArray(string input)
        {
            return input.Split(new char[]{ ' ',',' }, 
                StringSplitOptions
                .RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();
        }
    }
}
