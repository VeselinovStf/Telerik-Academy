﻿namespace SquareWithMaximumSum
{
    using System;
    using System.Linq;

    /// <summary>
    /// Write a program that read a matrix from console. Then find biggest sum of 2x2 
    /// submatrix and print it to console.
    /// 
    ///     On first line you will get matrix sizes in format rows, columns.
    /// One next rows lines you will get elements for each column separated with coma.
    /// Print biggest top-left square, which you find and sum of it's elements.
    /// </summary>
    public class StartUp
    {
        public static void Main()
        {
            string input = Console.ReadLine();

            int[] size = GetInputInArray(input);

            int rows = size[0];
            int cols = size[1];

            int[,] matrix = GetArrayLineToMatrix(rows, cols);

            int maxSum = 0;

            int[,] bestMatrix = new int[2, 2];

            for (int row = 0; row < matrix.GetLength(0) - 1; row++)
            {
                for (int col = 0; col < matrix.GetLength(1) - 1; col++)
                {
                    int sum = matrix[row, col] + matrix[row, col + 1] +
                        matrix[row + 1, col] + matrix[row + 1, col + 1];

                    if (sum > maxSum)
                    {
                        maxSum = sum;

                        bestMatrix[0, 0] = matrix[row, col];
                        bestMatrix[0, 1] = matrix[row, col+1];
                        bestMatrix[1, 0] = matrix[row + 1, col];
                        bestMatrix[1, 1] = matrix[row + 1, col + 1];

                    }

                }

            }

            PrintMatrix(bestMatrix);
            Console.WriteLine(maxSum);

        }

        private static void PrintMatrix(int[,] bestMatrix)
        {
            for (int r = 0; r < bestMatrix.GetLength(0); r++)
            {
                for (int c = 0; c < bestMatrix.GetLength(1); c++)
                {
                    Console.Write(bestMatrix[r, c] + " ");
                }
                Console.WriteLine();
            }
        }

        private static int[,] GetArrayLineToMatrix(int rows, int cols)
        {
            int[,] matrix = new int[rows, cols];

            for (int matrixRow = 0; matrixRow < rows; matrixRow++)
            {
                string input = Console.ReadLine();
                int[] inputRowOfMatrix = GetInputInArray(input);

                for (int matrixCol = 0; matrixCol < inputRowOfMatrix.Length; matrixCol++)
                {
                    matrix[matrixRow, matrixCol] = inputRowOfMatrix[matrixCol];

                }

            }

            return matrix;
        }

        private static int[] GetInputInArray(string input)
        {
            return input.Split(new char[] { ' ', ',' },
                StringSplitOptions
                .RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();
        }
    }
}

