﻿namespace DiagonalDifference
{
    using System;
    using System.Linq;

    /// <summary>
    /// Write a program that finds the difference between the 
    /// sums of the square matrix diagonals (absolute value).
    /// </summary>
    public class StartUp
    {
        public static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            int[,] matrix = new int[size, size];

            for (int r = 0; r < size; r++)
            {
                int[] inputRowForMatrix = Console.ReadLine().Split(new char[] { ' ' },
                    StringSplitOptions
                    .RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();

                for (int c = 0; c < size; c++)
                {
                    matrix[r, c] = inputRowForMatrix[c];
                }
            }

            int leftDiagonalSum = GetLeftDiagonalSum(matrix);
            int rightDiagonal = GetRightDiagonal(matrix);

            Console.WriteLine(Math.Abs(leftDiagonalSum - rightDiagonal));


        }

        private static int GetRightDiagonal(int[,] matrix)
        {
            int sum = 0;

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = matrix.GetLength(1) - 1; col >= 0; col--)
                {
                    sum += matrix[row, col];
                    row++;
                }
            }

            return sum;
        }

        private static int GetLeftDiagonalSum(int[,] matrix)
        {
            int sum = 0;

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; row < matrix.GetLength(0); col++)
                {
                    sum += matrix[row, col];
                    row++;
                }
            }

            return sum;
        }
    }
}
