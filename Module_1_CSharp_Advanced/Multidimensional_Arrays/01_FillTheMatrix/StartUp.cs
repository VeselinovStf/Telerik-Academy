﻿namespace _01_FillTheMatrix
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            int initialSize = int.Parse(Console.ReadLine());

            int[,] matrix = new int[initialSize, initialSize];

            char arrayType = char.Parse(Console.ReadLine());

            switch (arrayType)
            {
                case 'a':
                    matrix = CreateATypeMatrix(matrix);
                    break;
                case 'b':
                    matrix = CreateBTypeMatrix(matrix);
                    break;
                case 'c':
                    matrix = CreateCTypeMatrix(matrix);
                    break;
                case 'd':
                    matrix = CreateDTypeMatrix(matrix);
                    break;
                default:
                    break;
            }

            PrintMatrix(matrix);
        }

        private static int[,] CreateDTypeMatrix(int[,] matrix)
        {
            int size = matrix.GetLength(0);

            int count = 1;

            int majorRowCount = size - 2;
            int majorColCount = size;

            int totalRows = 0;
            int totalCols = 0;

            int totalSize = size * size;

            while (count < totalSize)
            {

                for (int leftSide = 0; leftSide < majorColCount; leftSide++)
                {
                    matrix[totalRows, totalCols] = count;
                    totalRows++;
                    count++;

                }

                totalRows--;
                totalCols++;

                for (int bottomSide = 0; bottomSide < majorRowCount; bottomSide++)
                {
                    matrix[totalRows, totalCols] = count;
                    totalCols++;
                    count++;
                }


                for (int rightSite = 0; rightSite < majorColCount; rightSite++)
                {
                    matrix[totalRows, totalCols] = count;
                    totalRows--;
                    count++;

                }

                totalRows++;
                totalCols--;

                for (int topSite = 0; topSite < majorRowCount; topSite++)
                {
                    matrix[totalRows, totalCols] = count;
                    totalCols--;
                    count++;

                }

                totalRows++;
                totalCols++;
                majorColCount -= 2;
                majorRowCount -= 2;

                if (count == totalSize)
                {
                    matrix[size / 2, size / 2] = count;
                }
            }
            return matrix;
        }

        private static int[,] CreateCTypeMatrix(int[,] matrix)
        {

            int leftBottomDiagonalLoopCount = matrix.GetLength(0);

            int rowStart = matrix.GetLength(0) - 1;
            int colCount = 0;

            int counter = 1;
            while (leftBottomDiagonalLoopCount > 0)
            {
                for (int i = rowStart; i <= matrix.GetLength(0) - 1; i++)
                {
                    matrix[i, colCount] = counter;
                    colCount++;
                    counter++;
                }

                colCount = 0;
                rowStart--;

                leftBottomDiagonalLoopCount--;
            }

            rowStart = 0;
            colCount = 1;

            int rigtBottomLoopCount = matrix.GetLength(0) - 2;

            while (rigtBottomLoopCount >= 0)
            {
                for (int i = colCount; i <= matrix.GetLength(1) - 1; i++)
                {
                    matrix[rowStart, i] = counter;
                    rowStart++;
                    counter++;
                }

                rowStart = 0;
                colCount++;

                rigtBottomLoopCount--;
            }




            return matrix;
        }

        private static int[,] CreateBTypeMatrix(int[,] matrix)
        {
            int count = 1;

            for (int col = 0; col < matrix.GetLength(1); col++)
            {

                if (col % 2 == 0)
                {
                    for (int row = 0; row < matrix.GetLength(0); row++)
                    {
                        matrix[row, col] = count;
                        count++;
                    }
                }
                else
                {
                    for (int row = matrix.GetLength(0) - 1; row >= 0; row--)
                    {
                        matrix[row, col] = count;
                        count++;
                    }
                }


            }


            return matrix;
        }

        private static void PrintMatrix(int[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {

                    Console.Write(matrix[row, col]);
                    if (col < matrix.GetLength(1) - 1)
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
        }

        private static int[,] CreateATypeMatrix(int[,] matrix)
        {
            int count = 1;

            for (int col = 0; col < matrix.GetLength(1); col++)
            {
                for (int row = 0; row < matrix.GetLength(0); row++)
                {
                    matrix[row, col] = count;
                    count++;
                }
            }


            return matrix;
        }
    }
}
