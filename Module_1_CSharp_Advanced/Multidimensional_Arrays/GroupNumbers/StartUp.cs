﻿namespace GroupNumbers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Read a set of numbers and group them by their remainder when dividing to 3 (0, 1 and 2).
    /// One first line, you will get numbers separated with coma and whitespace.
    /// </summary>
    public class StartUp
    {
        public static void Main()
        {
            int[] inputNumbers = Console.ReadLine()
                .Split(new char[] { ' ', ',' },
                StringSplitOptions
                .RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            int[] row0 = GetCurrentLength(inputNumbers, 0);
            int[] row1 = GetCurrentLength(inputNumbers, 1);
            int[] row2 = GetCurrentLength(inputNumbers, 2);

            int[][] resultJag = new int[][]
            {
                row0,
                row1,
                row2,
            };

            PrintMatrix(resultJag);

        }

        private static void PrintMatrix(int[][] resultJag)
        {
            for (int row = 0; row < resultJag.Length; row++)
            {
                for (int col = 0; col < resultJag[row].Length; col++)
                {
                    Console.Write(resultJag[row][col] + " ");
                }
                Console.WriteLine();
            }
        }

        private static int[] FillArray(int[] row)
        {
            int[] result = new int[] { };

            for (int i = 0; i < row.Length; i++)
            {
                result[i] = row[i];
            }

            return result;
        }


        private static int[] GetCurrentLength(int[] inputNumbers, int div)
        {
            List<int> count = new List<int>();

            int index = 0;

            for (int i = 0; i < inputNumbers.Length; i++)
            {
                int currentNumber = inputNumbers[i];

                if (Math.Abs(currentNumber) % 3 == div)
                {
                    count.Add(inputNumbers[i]);
                    index++;
                }

            }

            return count.ToArray();
        }
    }
}

