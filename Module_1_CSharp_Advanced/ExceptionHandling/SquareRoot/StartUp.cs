﻿namespace SquareRoot
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            string input = Console.ReadLine();

            try
            {
                double number = double.Parse(input);

                if (number < 0)
                {
                    throw new Exception();
                }

                double sqrtRoot = Math.Sqrt(number);

                Console.WriteLine("{0:F3}", sqrtRoot);
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid number");
            }
            finally
            {
                Console.WriteLine("Good bye");
            }
        }
    }
}
