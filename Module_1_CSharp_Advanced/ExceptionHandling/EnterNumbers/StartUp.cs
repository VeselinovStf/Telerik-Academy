﻿namespace EnterNumbers
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            const int totalInputNumsCount = 12;

            double[] numbers = new double[totalInputNumsCount];

            

            try
            {
                numbers = ReadNumber(1, totalInputNumsCount);

                CheckFirstSmallestThenSecond(numbers);

                Console.WriteLine(string.Join(" < ", numbers));
            }
            catch (Exception)
            {
                Console.WriteLine("Exception");
            }

        }

        private static void CheckFirstSmallestThenSecond(double[] numbers)
        {
            for (int i = 0; i < numbers.Length -1; i++)
            {
                if (numbers[i] >= numbers[i+1] || numbers[i] > 100)
                {
                    throw new Exception();
                }
            }
        }

        private static double[] ReadNumber(int v, int totalInputNumsCount)
        {
            double[] numbers = new double[totalInputNumsCount];

            numbers[0] = 1;
            numbers[numbers.Length - 1] = 100;

            for (int i = v; i < totalInputNumsCount-1; i++)
            {
                numbers[i] = double.Parse(Console.ReadLine());
            }

            return numbers;
        }
    }
}
