﻿namespace CorrectBracketsTests
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            string expresion = Console.ReadLine();

            int open = 0;
            int close = 0;

            for (int i = 0; i < expresion.Length; i++)
            {
                if (expresion[i] == '(')
                {
                    open++;
                }

                if (expresion[i] == ')')
                {
                    close++;
                }
            }

            if (open > close || close > open)
            {
                Console.WriteLine("Incorrect");
            }
            else
            {
                Console.WriteLine("Correct");
            }
        }
    }
}
