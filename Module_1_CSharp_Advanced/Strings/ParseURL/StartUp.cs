﻿namespace ParseURL
{
    using System;
    using System.Text.RegularExpressions;

    public class StartUp
    {
        public static void Main()
        {
            string inputUrl = Console.ReadLine();

            var fragments = Regex.Match(inputUrl, "(.*)://(.*?)(/.*)").Groups;


            Console.WriteLine("[protocol] = {0}", fragments[1]);
            Console.WriteLine("[server] = {0}", fragments[2]);
            Console.Write("[resource] = {0}\n", fragments[3]);
        }
    }
}
