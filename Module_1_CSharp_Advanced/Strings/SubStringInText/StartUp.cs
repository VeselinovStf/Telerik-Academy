﻿namespace SubStringInText
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class StartUp
    {
        public static void Main()
        {
            string pattern = Console.ReadLine().ToLower();

            string text = Console.ReadLine().ToLower();

            int i = 0;

            int count = 0;

            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                count++;
                i++;
            }

            Console.WriteLine(count);
        }

      
    }
}
