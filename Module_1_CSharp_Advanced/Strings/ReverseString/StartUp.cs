﻿namespace ReverseString
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            string inputStr = Console.ReadLine();

            string outputString = string.Empty;

            for (int i = inputStr.Length-1; i >= 0; i--)
            {
                outputString += inputStr[i];
            }

            Console.WriteLine(outputString);
            
        }
    }
}
