﻿namespace AppearanceCount
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            int arraySize = int.Parse(Console.ReadLine());

            string[] inputArrayNums = Console.ReadLine().Split(' ');

            int[] numbers = GetArrayFromInput(arraySize, inputArrayNums);

            int searchNumber = int.Parse(Console.ReadLine());

            int appearanceOfsearchNumber = GetTotalAppearanceOfNumber(searchNumber, numbers);

            Console.WriteLine(appearanceOfsearchNumber);
        }

        private static int GetTotalAppearanceOfNumber(int searchNumber, int[] numbers)
        {
            int count = 0;

            foreach (var num  in numbers)
            {
                if (num == searchNumber)
                {
                    count++;
                }
            }

            return count;
        }

        private static int[] GetArrayFromInput(int arraySize, string[] inputArrayNums)
        {
            int[] arr = new int[arraySize];

            for (int i = 0; i < inputArrayNums.Length; i++)
            {
                arr[i] = int.Parse(inputArrayNums[i]);
            }

            return arr;
        }
    }
}
