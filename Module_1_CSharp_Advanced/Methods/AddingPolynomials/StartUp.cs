﻿namespace AddingPolynomials
{
    using System;
    using System.Linq;

    public class StartUp
    {
        public static void Main()
        {

            int n = int.Parse(Console.ReadLine());

            int[] firstPolynomialCoefficients = Console.ReadLine()
                .Split(' ')
                .Select(int.Parse)
                .ToArray();

            int[] secondPolynomialCoefficients = Console.ReadLine()
                .Split(' ')
                .Select(int.Parse)
                .ToArray();

            int[] sum = SumArrays(firstPolynomialCoefficients, secondPolynomialCoefficients, n);

            Console.WriteLine(String.Join(" ", sum));
        }

        public static int[] SumArrays(int[] firstArray, int[] secondArray, int n)
        {
            int[] result = new int[n];

            for (int i = 0; i < n; i++)
            {
                result[i] = firstArray[i] + secondArray[i];
            }

            return result;
        }
    }
}

