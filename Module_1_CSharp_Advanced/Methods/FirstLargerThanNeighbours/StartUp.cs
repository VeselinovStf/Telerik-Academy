﻿namespace FirstLargerThanNeighbours
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            int arraySize = int.Parse(Console.ReadLine());

            string[] inputNumbers = Console.ReadLine().Split(' ');

            int[] numbers = GetArrayFromInput(arraySize, inputNumbers);

            int neighbourCount = -1;

            if (numbers.Length > 3)
            {
                neighbourCount = FindAllNeighbours(numbers);
            }

            Console.WriteLine(neighbourCount);
        }

        private static int FindAllNeighbours(int[] numbers)
        {
            int index = -1;

            for (int i = 1; i < numbers.Length - 1; i++)
            {
                int currentElement = numbers[i];

                if (numbers[i - 1] < currentElement && numbers[i + 1] < currentElement)
                {
                    index = i;
                    return index;
                }
            }

            return index;
        }

        private static int[] GetArrayFromInput(int arraySize, string[] inputArrayNums)
        {
            int[] arr = new int[arraySize];

            for (int i = 0; i < inputArrayNums.Length; i++)
            {
                arr[i] = int.Parse(inputArrayNums[i]);
            }

            return arr;
        }
    }
}
