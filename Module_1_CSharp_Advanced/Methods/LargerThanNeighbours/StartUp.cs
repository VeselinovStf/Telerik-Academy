﻿namespace LargerThanNeighbours
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            int arraySize = int.Parse(Console.ReadLine());

            string[] inputNumbers = Console.ReadLine().Split(' ');

            int[] numbers = GetArrayFromInput(arraySize, inputNumbers);

            int neighbourCount = 0;

            if (numbers.Length > 3)
            {
                neighbourCount += FindAllNeighbours(numbers);
            }

            Console.WriteLine(neighbourCount);
        }

        private static int FindAllNeighbours(int[] numbers)
        {
            int count = 0;

            for (int i = 1; i < numbers.Length - 1; i++)
            {
                int currentElement = numbers[i];

                if (numbers[i - 1] < currentElement && numbers[i+1] < currentElement)
                {
                    count++;
                }
            }

            return count;
        }

        private static int[] GetArrayFromInput(int arraySize, string[] inputArrayNums)
        {
            int[] arr = new int[arraySize];

            for (int i = 0; i < inputArrayNums.Length; i++)
            {
                arr[i] = int.Parse(inputArrayNums[i]);
            }

            return arr;
        }
    }
}
