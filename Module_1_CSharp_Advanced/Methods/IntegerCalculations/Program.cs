﻿namespace IntegerCalculations
{
  
        using System;
        using System.Linq;
        using System.Numerics;
      
        public class IntegerCalculations
        {
          
            public static void Main()
            {
                int[] numbers = Console.ReadLine()
                    .Split(new char[] { ' ' },
                    StringSplitOptions
                    .RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();

                int minimum = GetSmallestNumber(numbers);
                int maximum = GetBiggestNumber(numbers);
                double avarage = GetAvarageNumber(numbers);
                int sum = GetSumOfNumbers(numbers);
                BigInteger product = GetProductOfNumbers(numbers);

                Console.WriteLine(minimum);
                Console.WriteLine(maximum);
                Console.WriteLine("{0:F2}", avarage);
                Console.WriteLine(sum);
                Console.WriteLine(product);
            }

            public static BigInteger GetProductOfNumbers(int[] arr)
            {
                BigInteger product = arr[0];

                for (int i = 1; i < arr.Length; i++)
                {
                    product *= arr[i];
                }

                return product;
            }

            private static double GetAvarageNumber(int[] arr)
            {
                int sum = GetSumOfNumbers(arr);

                double avarage = (double)sum / arr.Length;

                return avarage;
            }

            public static int GetSumOfNumbers(int[] arr)
            {
                int sum = 0;

                for (int i = 0; i < arr.Length; i++)
                {
                    sum += arr[i];

                }

                return sum;
            }

            public static int GetBiggestNumber(int[] arr)
            {
                int biggest = int.MinValue;

                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] > biggest)
                    {
                        biggest = arr[i];
                    }
                }

                return biggest;
            }

            public static int GetSmallestNumber(int[] arr)
            {
                int smallest = int.MaxValue;

                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] < smallest)
                    {  
                        smallest = arr[i];
                    }
                }

                return smallest;
            }
        }
    
}
