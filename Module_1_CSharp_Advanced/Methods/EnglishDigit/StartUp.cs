﻿namespace EnglishDigit
{
    using System;

    public class StartUp
    {
         
        public static void Main()
        {
            int inputNumber = int.Parse(Console.ReadLine());

            int lastNumber = GetLastNumber(inputNumber);

            string[] numbersInEnglish = new string[]
        {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine"
        };

            string lastNumberInEnglish = NumberToEnglish(lastNumber, numbersInEnglish);

            Console.WriteLine(lastNumberInEnglish);
        }

        private static string NumberToEnglish(int lastNumber, string[] numbersInEnglish)
        {
            return numbersInEnglish[lastNumber];
        }

        private static int GetLastNumber(int inputNumber)
        {
            return inputNumber % 10;
        }
    }
}
