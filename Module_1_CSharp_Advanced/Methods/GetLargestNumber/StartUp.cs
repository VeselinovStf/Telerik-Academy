﻿namespace GetLargestNumber
{
    using System;
    using System.Linq;

    public class StartUp
    {
        public static void Main()
        {
            string inputNumbers = Console.ReadLine();

            int[] numbersArr = GetInputNumsToArray(inputNumbers);

            int maxNumber = GetMax(numbersArr[0], numbersArr[1]);

            maxNumber = GetMax(maxNumber, numbersArr[2]);

            Console.WriteLine(maxNumber);
        }

        private static int GetMax(int firstNUmber, int secondNumber)
        {
            if (firstNUmber > secondNumber)
            {
                return firstNUmber;
            }
            return secondNumber;
        }

        private static int[] GetInputNumsToArray(string inputNumbers)
        {
            return inputNumbers.Split(new char[] { ' ' }
           , StringSplitOptions
           .RemoveEmptyEntries)
           .Select(int.Parse)
           .ToArray();
        }
           


    }
}
