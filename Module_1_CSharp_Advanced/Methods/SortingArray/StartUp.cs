﻿namespace SortingArray
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            int arrayInputSize = int.Parse(Console.ReadLine());

            string[] inputArray = Console.ReadLine().Split(' ');

            int[] numbersToSort = GetInputToArray(arrayInputSize, inputArray);

            bool swape; ;

            do
            {
                swape = false;
                for (int i = 0; i < numbersToSort.Length-1; i++)
                {
                    if (isBiggest(numbersToSort[i],numbersToSort[i+1]))
                    {
                        swape = true;
                        Swap(i, i + 1, numbersToSort);

                    }
                }

            } while (swape);

            Console.WriteLine(string.Join(" ", numbersToSort));
        }

        private static void Swap(int i, int v, int[] numbersToSort)
        {
            int temp = numbersToSort[i];
            numbersToSort[i] = numbersToSort[v];
            numbersToSort[v] = temp;
        }

        private static bool isBiggest(int v1, int v2)
        {
            if (v1 > v2)
            {
                return true;
            }

            return false;
        }

        private static int[] GetInputToArray(int arrayInputSize, string[] inputArray)
        {
            int[] arr = new int[arrayInputSize];

            for (int i = 0; i < arrayInputSize; i++)
            {
                arr[i] = int.Parse(inputArray[i]);
            }

            return arr;
        }
    }
}
