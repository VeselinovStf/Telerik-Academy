﻿namespace ReverseNumber
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            string inputDecimalNumber = Console.ReadLine();

            string reversedDecimalNums = ReversDecimalNumber(inputDecimalNumber);

            Console.WriteLine(reversedDecimalNums);
        }

        private static string ReversDecimalNumber(string inputDecimalNumber)
        {
            string result = string.Empty;

            for (int i = inputDecimalNumber.Length -1 ; i >= 0 ; i--)
            {
                result += inputDecimalNumber[i];
            }

            return result;
        }
    }
}
