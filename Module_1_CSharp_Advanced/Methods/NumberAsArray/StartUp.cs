﻿namespace NumberAsArray
{
    using System;
    using System.Linq;

    public class StartUp
    {
        public static void Main()
        {
            string arraysLenghts = Console.ReadLine();

            int[] numericArraysLength = 
                GetInputStringToArray(arraysLenghts);

            int firstArraySize = numericArraysLength[0];
            int secondArraySize = numericArraysLength[1];

            string[] inputStringArray = 
                Console.ReadLine().Split(' ');

            int[] firstNumericArray = 
                GetInputStringToArray(inputStringArray, firstArraySize);

            string[] secondInputStringArray =
                Console.ReadLine().Split(' ');

            int[] secondNumericArray =
                GetInputStringToArray(secondInputStringArray, secondArraySize);

            int[] summedArrays = SumTwoArrays(firstNumericArray, secondNumericArray);

            SumBiggerThenNine(summedArrays);
            

            Console.WriteLine(string.Join(" ", summedArrays));

        }

        private static void SumBiggerThenNine(int[] summedArrays)
        {
            for (int i = 0; i < summedArrays.Length - 1; i++)
            {
                if (summedArrays[i] > 9)
                {
                    int lastDigit = summedArrays[i] % 10;
                    int difference = summedArrays[i] / 10;


                    summedArrays[i] = lastDigit;

                    summedArrays[i + 1] = summedArrays[i + 1] + difference;
                }

            }
        }

        private static int[] SumTwoArrays(int[] firstNumericArray, int[] secondNumericArray)
        {

            if (firstNumericArray.Length > secondNumericArray.Length)
            {
                for (int i = 0; i < secondNumericArray.Length; i++)
                {
                    firstNumericArray[i] += secondNumericArray[i];
                }

                return firstNumericArray;
            }
            else
            {
                for (int i = 0; i < firstNumericArray.Length; i++)
                {
                    secondNumericArray[i] += firstNumericArray[i];
                }

                return secondNumericArray;
            }


            
        }

        private static int[] GetInputStringToArray(string[] inputStringArray, int arraySize)
        {
            int[] arr = new int[arraySize];

            for (int i = 0; i < arraySize; i++)
            {
                arr[i] = int.Parse(inputStringArray[i]);
            }

            return arr;
        }

        private static int[] GetInputStringToArray(string arraysLenghts)
        {
            return arraysLenghts
                .Split(' ')
                .Select(int.Parse)
                .ToArray();
        }
    }
}
