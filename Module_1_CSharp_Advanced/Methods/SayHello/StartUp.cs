﻿namespace SayHello
{
    using System;

    public class StartUp
    {
        public static void Main()
        {
            string userName = Console.ReadLine();

            PrintName(userName);
            
        }

        private static void PrintName(string userName)
        {
            Console.WriteLine("Hello, {0}!", userName);
        }
    }
}
